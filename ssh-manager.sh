#!/bin/bash

if [ $# -lt 1 ]; then
    printf 'Usage: '`basename $0`' <command> \n\n'
    printf 'Avaliable commands: \n'
    printf '   list       Show the list of servers \n'
    printf '   connect    Connect to server \n'
    exit 1
fi

key="$1"

case $key in
	list)
		printf 'Not implemented \n'
	;;

	--version)
		printf '0.0.1\n'
	;;

	connect)
		printf 'Not implemented\n'
	;;

	*)
		echo $1 is uknown command
	;;
esac
